#include "src/summator.hh"
#include <emscripten.h>
#include <emscripten/bind.h>
#include <iostream>

using namespace emscripten;

EMSCRIPTEN_BINDINGS(Summator) {
    class_<Summator>("Summator")
        .constructor()
        .function("sum", &Summator::sum);
}

int main() {
    std::cout << "Hello World! from main()\n";
}
