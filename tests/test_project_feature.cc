#include "src/summator.hh"
#include <gtest/gtest.h>

TEST(Summator, summator) {
    Summator s;
    ASSERT_EQ(s.sum(2, 3), 5);
}
