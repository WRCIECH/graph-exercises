import createMyModule from '@/../SummatorForWeb.js'

let Module;

createMyModule().then(MyModule => {
    Module = MyModule;
    postMessage({
        command: 'moduleInitialized'
    })
});

const getSum = (a, b) => {
    const summator = new Module.Summator();
    const sum = summator.sum(a, b);
    postMessage({
        command: 'sumResult',
        value: sum
    })
}
addEventListener('message', function(e) {
    const command = e.data.command;
    if (command) {
        if(command === 'getSum') {
            getSum(e.data.firstNumber, e.data.secondNumber)
        }
    }
});